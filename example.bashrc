alias ls="ls -G"
alias l=ls
alias ll="ls -lh"
alias la="ls -a"
alias df="df -h"
alias clean='rm -fv $(find . -name *~)'

# Git aliases
alias gl='git log --pretty="%Cblue%h %Creset%cr %Cred%s"'
alias glh='gl | head'
alias gs='git status'

export PAGER=less
export EDITOR=vim

PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
export PS1=$PS1
