function! AddHeader(ext, type)
  if a:type == 'Makefile'
    silent! execute '0r ' . $HOME . '/.vim/templates/header_template.makefile'
    silent! execute 'call ReplaceTemplateTags()'
  elseif a:type == 'normal'
    silent! execute '0r ' . $HOME . '/.vim/templates/header_template.' . a:ext
    silent! execute 'call ReplaceTemplateTags()'
  endif
endfunc

source $HOME/.vim/templates/replace_template_tags.vim

autocmd BufNewFile	*		silent! call AddHeader('%:e', 'normal')
autocmd BufRead		*		silent! call AddHeader('%:e', 'nothing')
autocmd BufNewFile	Makefile	silent! call AddHeader('%:e', 'Makefile')
autocmd BufRead		Makefile	silent! call AddHeader('%:e', 'nothing')
