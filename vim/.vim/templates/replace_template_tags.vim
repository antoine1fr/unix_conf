" Look for the user's name :
if exists("$USERNAME")
  let s:user_name = $USERNAME
elseif exists("$LOGNAME")
  let s:user_name = '%USER_NAME%'
endif

" Look for the user's login "
if exists("$LOGIN")
  let s:user_login = $LOGIN
elseif exists("LOGNAME")
  let s:user_login = $LOGNAME
else
  let s:user_login = '%USER_LOGIN%'
endif

" Look for the user's email :
if exists("$MAIL")
  let s:user_mail = $MAIL
else
  let s:user_mail = '%USER_EMAIL%'
endif

" Sun May  9 01:05:57 2010
function! ReplaceTemplateTags()
  execute '%s,%USER_NAME%,'	. s:user_name		. ',ge'
  execute '%s,%USER_EMAIL%,'	. s:user_mail		. ',ge'
  execute '%s,%USER_LOGIN%,'	. s:user_login		. ',ge'
  execute '%s,%FILE_PATH%,'	. expand('%:p:h')	. ',ge'
  execute '%s,%FILE_NAME%,'	. expand('%:t')		. ',ge'
  execute '%s,%CURRENT_DATE%,'	. strftime('%a %b %d %H:%M:%S %Y')	. ',ge'
endfun
